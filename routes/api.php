<?php

use Illuminate\Http\Request;
use App\Http\Controllers\API\MarcaController;
use App\Http\Controllers\API\TallaController;
use App\Http\Controllers\API\ProductoController;

Route::get('/marca', [MarcaController::class, 'show']);
Route::get('/marca/{id}', [MarcaController::class, 'showOne']);
Route::post('/marca', [MarcaController::class, 'create']);
Route::put('/marca/{id}', [MarcaController::class, 'edit']);
Route::delete('/marca/{id}', [MarcaController::class, 'delete']);


Route::get('/talla', [TallaController::class, 'show']);
Route::post('/talla', [TallaController::class, 'create']);
Route::put('/talla/{id}', [TallaController::class, 'edit']);
Route::delete('/talla/{id}', [TallaController::class, 'delete']);

Route::get('/producto', [ProductoController::class, 'show']);
Route::get('/producto/{id}', [ProductoController::class, 'showOne']);
Route::post('/producto', [ProductoController::class, 'create']);
Route::put('/producto/{id}', [ProductoController::class, 'edit']);
Route::delete('/producto/{id}', [ProductoController::class, 'delete']);
