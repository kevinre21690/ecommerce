<?php

namespace App\Http\Controllers\Api;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Services\ProductoService;
use App\DTO\ProductoDTO;
use App\DTO\ProductoResponseDTO;
use Illuminate\Support\Facades\Validator;

class ProductoController extends Controller
{
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|string',
            'observaciones' => 'required|string',
            'cantidad' => 'required|integer',
            'fecha_embarque' => 'required|date',
            'idtalla' => 'required|integer|exists:talla,id',
            'idmarca' => 'required|integer|exists:marca,id',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $productoDTO = new ProductoDTO(
            $request->nombre,
            $request->observaciones,
            $request->cantidad,
            $request->fecha_embarque,
            $request->idtalla,
            $request->idmarca
        );

        $response = ProductoService::createProducto($productoDTO);

        return response()->json($response->toArray(), $response->status_code);
    }

    public function edit(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|string',
            'observaciones' => 'required|string',
            'cantidad' => 'required|integer',
            'fecha_embarque' => 'required|date',
            'idtalla' => 'required|integer|exists:talla,id',
            'idmarca' => 'required|integer|exists:marca,id',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $productoDTO = new ProductoDTO(
            $request->nombre,
            $request->observaciones,
            $request->cantidad,
            $request->fecha_embarque,
            $request->idtalla,
            $request->idmarca
        );

        $response = ProductoService::editProducto($id, $productoDTO);

        return response()->json($response->toArray(), $response->status_code);
    }

    public function delete($id)
    {
        $response = ProductoService::deleteProducto($id);

        return response()->json($response->toArray(), $response->status_code);
    }

    public function show()
    {
        $response = ProductoService::showProducto();

        return response()->json($response->toArray(), $response->status_code);
    }

    public function showOne($id)
    {
        $response = ProductoService::showOneProducto($id);

        return response()->json($response->toArray(), $response->status_code);
    }
}