<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Services\MarcaService;
use App\DTO\MarcaDTO;
use App\DTO\MarcaResponseDTO;
use Illuminate\Support\Facades\Validator;
use Illuminate\Routing\Controller;


class MarcaController extends Controller
{
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|string',
            'referencia' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $marcaDTO = new MarcaDTO($request->nombre, $request->referencia);
        $response = MarcaService::createMarca($marcaDTO);

        return response()->json($response->toArray(), $response->status_code);
    }

    public function edit(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|string',
            'referencia' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $marcaDTO = new MarcaDTO($request->nombre, $request->referencia);
        $response = MarcaService::editMarca($id, $marcaDTO);

        return response()->json($response->toArray(), $response->status_code);
    }

    public function delete($id)
    {
        $response = MarcaService::deleteMarca($id);

        return response()->json($response->toArray(), $response->status_code);
    }

    public function show()
    {
        $response = MarcaService::showMarca();

        return response()->json($response->toArray(), $response->status_code);
    }

    public function showOne($id)
    {
        $response = MarcaService::showOneMarca($id);

        return response()->json($response->toArray(), $response->status_code);
    }

}
