<?php

namespace App\Http\Controllers\Api;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Services\TallaService;
use App\DTO\TallaDTO;
use App\DTO\TallaResponseDTO;
use Illuminate\Support\Facades\Validator;

class TallaController extends Controller
{
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $tallaDTO = new TallaDTO($request->nombre);
        $response = TallaService::createTalla($tallaDTO);

        return response()->json($response->toArray(), $response->status_code);
    }

    public function edit(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $tallaDTO = new TallaDTO($request->nombre);
        $response = TallaService::editTalla($id, $tallaDTO);

        return response()->json($response->toArray(), $response->status_code);
    }

    public function delete($id)
    {
        $response = TallaService::deleteTalla($id);
        return response()->json($response->toArray(), $response->status_code);
    }

    public function show()
    {
        $response = TallaService::showTalla();
        return response()->json($response->toArray(), $response->status_code);
    }
}
