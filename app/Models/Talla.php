<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Talla extends Model
{
    protected $table = 'talla';

    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'referencia'];

    // Relación: Una talla puede tener muchos productos
    public function productos(): HasMany
    {
        return $this->hasMany(Producto::class);
    }
}
