<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    use HasFactory;

    protected $table = 'producto';

    protected $fillable = [
        'nombre',
        'observaciones',
        'cantidad',
        'fecha_embarque',
        'id_talla',
        'id_marca'
    ];

    // Relación con la tabla talla
    public function talla()
    {
        return $this->belongsTo(Talla::class, 'id_talla');
    }

    // Relación con la tabla marca
    public function marca()
    {
        return $this->belongsTo(Marca::class, 'id_marca');
    }
}
