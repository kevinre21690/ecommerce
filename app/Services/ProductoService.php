<?php

namespace App\Services;

use App\Models\Producto;
use App\DTO\ProductoDTO;
use App\DTO\ProductoResponseDTO;
use App\DTO\ProductoEstandarResponseDTO;
use Illuminate\Http\Response;

class ProductoService
{
    public static function createProducto(ProductoDTO $productoDTO): ProductoResponseDTO
    {
        $producto = Producto::create([
            'nombre' => $productoDTO->nombre,
            'observaciones' => $productoDTO->observaciones,
            'cantidad' => $productoDTO->cantidad,
            'fecha_embarque' => $productoDTO->fecha_embarque,
            'id_talla' => $productoDTO->idtalla,
            'id_marca' => $productoDTO->idmarca,
        ]);

        if ($producto) {
            return new ProductoResponseDTO(true, Response::HTTP_CREATED, __('Producto creado exitosamente'), [$producto]);
        } else {
            return new ProductoResponseDTO(false, Response::HTTP_BAD_REQUEST, __('Error al crear el producto'));
        }
    }

    public static function editProducto(int $id, ProductoDTO $productoDTO): ProductoResponseDTO
    {
        $producto = Producto::find($id);

        if (!$producto) {
            return new ProductoResponseDTO(false, Response::HTTP_NOT_FOUND, __('Producto no encontrado'));
        }

        $producto->nombre = $productoDTO->nombre;
        $producto->observaciones = $productoDTO->observaciones;
        $producto->cantidad = $productoDTO->cantidad;
        $producto->fecha_embarque = $productoDTO->fecha_embarque;
        $producto->id_talla = $productoDTO->idtalla;
        $producto->id_marca = $productoDTO->idmarca;
        $producto->save();


        return new ProductoResponseDTO(true, Response::HTTP_OK, __('Producto actualizado exitosamente'), [$producto]);
    }

    public static function deleteProducto(int $id): ProductoResponseDTO
    {
        $producto = Producto::find($id);

        if (!$producto) {
            return new ProductoResponseDTO(false, Response::HTTP_NOT_FOUND, __('Producto no encontrado'));
        }

        $producto->delete();

        return new ProductoResponseDTO(true, Response::HTTP_OK, __('Producto eliminado exitosamente'));
    }

    public static function showProducto(): ProductoResponseDTO
    {
        $productos = Producto::join('marca', 'producto.id_marca', '=', 'marca.id')
        ->join('talla', 'producto.id_talla', '=', 'talla.id')
        ->select(
            'producto.id',
            'producto.nombre',
            'producto.observaciones',
            'producto.cantidad',
            'producto.fecha_embarque',
            'producto.id_talla',
            'producto.id_marca',
            'marca.nombre as nombre_marca',
            'talla.nombre as nombre_talla'
        )
        ->get();

        $data = [];
        foreach ($productos as $producto) {
            $data[] = new ProductoEstandarResponseDTO(
                $producto->id,
                $producto->nombre,
                $producto->observaciones,
                $producto->cantidad,
                $producto->fecha_embarque,
                $producto->id_talla,
                $producto->id_marca,
                $producto->nombre_marca,
                $producto->nombre_talla


            );
        }

        return new ProductoResponseDTO(true, Response::HTTP_OK, __('Productos encontrados'), [$data]);
    }

    public static function showOneProducto(int $id): ProductoResponseDTO
    {
        $producto = Producto::join('marca', 'producto.id_marca', '=', 'marca.id')
        ->join('talla', 'producto.id_talla', '=', 'talla.id')
        ->select(
            'producto.id',
            'producto.nombre',
            'producto.observaciones',
            'producto.cantidad',
            'producto.fecha_embarque',
            'producto.id_talla',
            'producto.id_marca',
            'marca.nombre as nombre_marca',
            'talla.nombre as nombre_talla'
        )->where('producto.id', $id)->first();

        if (!$producto) {
            return new ProductoResponseDTO(false, Response::HTTP_NOT_FOUND, __('Producto no encontrado'));
        }

        $data[] = new ProductoEstandarResponseDTO(
            $producto->id,
            $producto->nombre,
            $producto->observaciones,
            $producto->cantidad,
            $producto->fecha_embarque,
            $producto->id_talla,
            $producto->id_marca,
            $producto->nombre_marca,
            $producto->nombre_talla
        );

         return new ProductoResponseDTO(true, Response::HTTP_OK, __('Producto encontrado'), [$data]);
    }
}
