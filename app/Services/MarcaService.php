<?php

namespace App\Services;

use App\Models\Marca;
use App\DTO\MarcaDTO;
use App\DTO\MarcaResponseDTO;
use App\DTO\MarcaEstandarResponseDTO;
use Illuminate\Http\Response;

class MarcaService
{
    public static function createMarca(MarcaDTO $marcaDTO): MarcaResponseDTO
    {
        $service = new self();

        if( $service->existMarca($marcaDTO->nombre)){
            return new MarcaResponseDTO(false, Response::HTTP_BAD_REQUEST, __('No pueden existir dos marcas con el mismo nombre'));
        }

        $marca = Marca::create([
            'nombre' => $marcaDTO->nombre,
            'referencia' => $marcaDTO->referencia,
        ]);

        if ($marca) {
            return new MarcaResponseDTO(true, Response::HTTP_CREATED, __('Marca creada exitosamente'), [$marca]);
        } else {
            return new MarcaResponseDTO(false, Response::HTTP_BAD_REQUEST, __('Error al crear la marca'));
        }
    }

    public function existMarca(string $nombre): bool
    {
        $marca = Marca::where('nombre', $nombre)->first();
        return $marca !== null;
    }

    public static function editMarca(int $id, MarcaDTO $marcaDTO): MarcaResponseDTO
    {
        $marca = Marca::find($id);

        if (!$marca) {
            return new MarcaResponseDTO(false, Response::HTTP_NOT_FOUND, __('Marca no encontrada'));
        }

        $marca->nombre = $marcaDTO->nombre;
        $marca->referencia = $marcaDTO->referencia;
        $marca->save();

        return new MarcaResponseDTO(true, Response::HTTP_OK, __('Marca actualizada exitosamente'), [$marca]);
    }

    public static function deleteMarca(int $id): MarcaResponseDTO
    {
        $marca = Marca::find($id);
        if (!$marca) {
            return new MarcaResponseDTO(false, Response::HTTP_NOT_FOUND, __('Marca no encontrada'));
        }

        $marca->delete();

        return new MarcaResponseDTO(true, Response::HTTP_OK, __('Marca eliminada exitosamente'));
    }

    public static function showMarca(): MarcaResponseDTO
    {
        $marcas = Marca::all();

        $data = [];
        foreach ($marcas as $marca) {
            $data[] = new MarcaEstandarResponseDTO(
                $marca->id,
                $marca->nombre,
                $marca->referencia
              
            );
        }
        return new MarcaResponseDTO(true, Response::HTTP_OK, __('Marcas encontradas'), [$data]);
    }

    public static function showOneMarca(int $id): MarcaResponseDTO
    {
        $marca = Marca::find($id);
        if (!$marca) {
            return new MarcaResponseDTO(false, Response::HTTP_NOT_FOUND, __('Marca no encontrada'));
        }
        $data = new MarcaEstandarResponseDTO(
            $marca->id,
            $marca->nombre,
            $marca->referencia
        );
        return new MarcaResponseDTO(true, Response::HTTP_OK, __('Marca encontrada'), [$data]);
    }
}
