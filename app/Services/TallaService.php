<?php

namespace App\Services;

use App\Models\Talla;
use App\DTO\TallaDTO;
use App\DTO\TallaResponseDTO;
use Illuminate\Http\Response;

class TallaService
{
    public static function createTalla(TallaDTO $tallaDTO): TallaResponseDTO
    {
        $talla = Talla::create([
            'nombre' => $tallaDTO->nombre,
        ]);

        if ($talla) {
            return new TallaResponseDTO(true, Response::HTTP_CREATED, __('Talla creada exitosamente'), $talla);
        } else {
            return new TallaResponseDTO(false, Response::HTTP_BAD_REQUEST, __('Error al crear la talla'));
        }
    }

    public static function editTalla(int $id, TallaDTO $tallaDTO): TallaResponseDTO
    {
        $talla = Talla::find($id);

        if (!$talla) {
            return new TallaResponseDTO(false, Response::HTTP_NOT_FOUND, __('Talla no encontrada'));
        }

        $talla->nombre = $tallaDTO->nombre;
        $talla->save();

        return new TallaResponseDTO(true, Response::HTTP_OK, __('Talla actualizada exitosamente'), [$talla]);
    }

    public static function deleteTalla(int $id): TallaResponseDTO
    {
        $talla = Talla::find($id);

        if (!$talla) {
            return new TallaResponseDTO(false, Response::HTTP_NOT_FOUND, __('Talla no encontrada'));
        }

        $talla->delete();

        return new TallaResponseDTO(true, Response::HTTP_OK, __('Talla eliminada exitosamente'));
    }

    public static function showTalla(): TallaResponseDTO
    {
        $tallas = Talla::all();

        $data = [];
        foreach ($tallas as $talla) {
            $data[] = new TallaDTO(
                $talla->id,
                $talla->nombre
            );
        }

        return new TallaResponseDTO(true, Response::HTTP_OK, __('Tallas encontradas'), $data);
    }
}
