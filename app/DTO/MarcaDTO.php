<?php

declare(strict_types=1);

namespace App\DTO;

use Spatie\DataTransferObject\DataTransferObject;

class MarcaDTO extends DataTransferObject
{
    public string $nombre;
    public string $referencia;

    public function __construct($nombre, $referencia)
    {
        $this->nombre = $nombre;
        $this->referencia = $referencia;
    }
}