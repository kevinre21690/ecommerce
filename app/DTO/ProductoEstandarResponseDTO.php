<?php

declare(strict_types=1);

namespace App\DTO;

use Spatie\DataTransferObject\DataTransferObject;


class ProductoEstandarResponseDTO extends DataTransferObject
{

    public $id;
    public $nombre;
    public $observaciones;
    public $cantidad;
    public $fecha_embarque;
    public $idtalla;
    public $idmarca;
    public $talla;
    public $marca;

    public function __construct($id, $nombre, $observaciones, $cantidad, $fecha_embarque, $idtalla, $idmarca, $marca, $talla)
    {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->observaciones = $observaciones;
        $this->cantidad = $cantidad;
        $this->fecha_embarque = $fecha_embarque;
        $this->idtalla = $idtalla;
        $this->idmarca = $idmarca;
        $this->marca= $marca;
        $this->talla = $talla;
    }
    
}