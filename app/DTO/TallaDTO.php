<?php

declare(strict_types=1);
namespace App\DTO;
use Spatie\DataTransferObject\DataTransferObject;

class TallaDTO extends DataTransferObject
{
    public string $nombre;
    public int $id;


    public function __construct( int $id, string $nombre)
    {
        $this->id = $id;
        $this->nombre = $nombre;
    }
}
