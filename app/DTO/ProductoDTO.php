<?php

declare(strict_types=1);

namespace App\DTO;

use Spatie\DataTransferObject\DataTransferObject;


class ProductoDTO extends DataTransferObject
{


    public $nombre;
    public $observaciones;
    public $cantidad;
    public $fecha_embarque;
    public $idtalla;
    public $idmarca;

    public function __construct($nombre, $observaciones, $cantidad, $fecha_embarque, $idtalla, $idmarca)
    {
        $this->nombre = $nombre;
        $this->observaciones = $observaciones;
        $this->cantidad = $cantidad;
        $this->fecha_embarque = $fecha_embarque;
        $this->idtalla = $idtalla;
        $this->idmarca = $idmarca;
    }
    
}