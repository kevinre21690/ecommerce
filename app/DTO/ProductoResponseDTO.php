<?php

declare(strict_types=1);
namespace App\DTO;

use Illuminate\Http\Response;
use App\Models\Producto;
use Spatie\DataTransferObject\DataTransferObject;

class ProductoResponseDTO extends DataTransferObject
{
    public bool $success;
    public int $status_code;
    public string $message;
    public ?array $data;

    public function __construct(bool $success, int $status_code, string $message, ?array $data = null)
    {
        $this->success = $success;
        $this->status_code = $status_code;
        $this->message = $message;
        $this->data = $data;
    }
}
