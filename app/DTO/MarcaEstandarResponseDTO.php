<?php

declare(strict_types=1);

namespace App\DTO;

use Spatie\DataTransferObject\DataTransferObject;

class MarcaEstandarResponseDTO extends DataTransferObject
{
    public int $id;
    public string $nombre;
    public string $referencia;

    public function __construct($id, $nombre, $referencia)
    {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->referencia = $referencia;
    }
}