<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearEcommerce extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     
        Schema::create('marca', function (Blueprint $table) {
            $table->id(); 
            $table->text('referencia'); 
            $table->string('nombre'); 
            $table->timestamps(); 
        });

        Schema::create('talla', function (Blueprint $table) {
            $table->id(); 
            $table->string('nombre'); 
            $table->text('referencia'); 
            $table->timestamps(); 
        });

        Schema::create('producto', function (Blueprint $table) {
            $table->id(); 
            $table->string('nombre'); 
            $table->text('observaciones')->nullable(); 
            $table->integer('cantidad')->unsigned(); 
            $table->date('fecha_embarque');
            $table->unsignedBigInteger('idtalla'); 
            $table->unsignedBigInteger('idmarca'); 
            $table->timestamps(); 

            $table->foreign('idtalla')->references('id')->on('talla');
            $table->foreign('idmarca')->references('id')->on('marca');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producto');

        Schema::dropIfExists('talla');

        Schema::dropIfExists('marca');
    }
}
